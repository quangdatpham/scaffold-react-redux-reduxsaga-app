import { all, take, put, call } from 'redux-saga/effects';

export function* helloSaga() {
  console.log('Hello Saga');
}

export default function* rootSaga() {
  yield all([
    helloSaga()
  ])
}