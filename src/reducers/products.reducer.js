import { UPDATE_PRODUCT,ADD_PRODUCT, DELETE_PRODUCT } from '../actions/products.action';

export default (state = [], action) => {
  const { _id, name, price } = action;

  switch (action.type) {
    case ADD_PRODUCT:
      return [
        ...state,
        {
          _id, name, price
        }
      ];
    case DELETE_PRODUCT:
      return state.filter(p => p._id !== _id);
    case UPDATE_PRODUCT:
      const newState = state.slice();
      const product = newState.find(p => p._id === _id);

      product.name = name;
      product.price = price;

      return newState;
    default:
      return state;
  }
}