import { CHANGE_LOCALE } from '../actions/app.action';

export default (state = {}, action) => {
  switch (action.type) {
    case CHANGE_LOCALE:
      return {
        ...state,
        locale: action.locale
      }
    default:
      return state;
  }
};