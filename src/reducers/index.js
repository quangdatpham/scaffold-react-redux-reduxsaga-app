import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import products from './products.reducer';
import app from './app.reducer';

const rootReducer = combineReducers({
  app,
  products,
  routing: routerReducer
});

export default rootReducer;
