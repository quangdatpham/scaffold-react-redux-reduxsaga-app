import React from 'react';
import Nav from '../Nav';

export default function DefaultLayout(props) {
  return (
    <div>
      <div>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
      </div>

      <Nav />

      {props.children}
    </div>
  );
}