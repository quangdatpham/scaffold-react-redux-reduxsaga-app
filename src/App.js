import React from 'react';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';

import Home from './pages/Home';

import store from './store';
import messages from './i18n';

import './styles/styles.scss';

function App() {
  const locale = store.getState().app.locale || 'en-US';
  const [, message] = Object.entries(messages).find(([l,]) => locale.toLocaleLowerCase().includes(l));

  return (
    <Provider store={store}>
      <IntlProvider {...{ locale, messages: message }}>
        <Home />
      </IntlProvider>
    </Provider>
  );
}

export default App;
