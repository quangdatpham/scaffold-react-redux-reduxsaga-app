import * as appActionCreators from './app.action';
import * as productActionCreators from './products.action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const stateActionMaps = {
  'products': productActionCreators,
  'app': appActionCreators
}

export default {
  appActionCreators,
  productActionCreators,
  ...(() => {
    const actionCreatorConnects = {};

    const from = REDUCER_NAME => {
      const mapStateToProps = state => ({ [REDUCER_NAME]: state[REDUCER_NAME] });
      const mapDispatchToProps = dispatch => bindActionCreators(stateActionMaps[REDUCER_NAME], dispatch);

      return C => connect(mapStateToProps, mapDispatchToProps)(C);
    }

    for (let p in stateActionMaps) {
      const reducerName = p[1].toUpperCase() + p.slice(1);
      actionCreatorConnects[`connectWith${reducerName}ActionCreators`] = from(p);
    }

    return actionCreatorConnects;
  })()
};